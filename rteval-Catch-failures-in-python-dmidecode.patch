From 606bb31cbf967e5e48c10c7e8ea8bab0685eb410 Mon Sep 17 00:00:00 2001
From: John Kacur <jkacur@redhat.com>
Date: Thu, 2 Feb 2023 00:47:31 -0500
Subject: [PATCH] rteval: Catch failures in python-dmidecode

python-dmidecode can generate incorrect xml,
namely Attribute unit redefined

Although useful, the dmidecode is not critical to rteval reporting.

Therefore catch this, and first see if we can at least query the bios.
If that works report the bios instead off all, and if that
doesn't work, just continue without the dmidecode report.

Signed-off-by: John Kacur <jkacur@redhat.com>
---
 rteval/sysinfo/dmi.py | 14 +++++++++++++-
 1 file changed, 13 insertions(+), 1 deletion(-)

diff --git a/rteval/sysinfo/dmi.py b/rteval/sysinfo/dmi.py
index 83f347623b58..89a7faae06b1 100644
--- a/rteval/sysinfo/dmi.py
+++ b/rteval/sysinfo/dmi.py
@@ -79,6 +79,7 @@ class DMIinfo:
 
     def __init__(self, logger=None):
         self.__version = '0.6'
+        self._log = logger
 
         if not dmidecode_avail:
             logger.log(Log.DEBUG, "DMI info unavailable, ignoring DMI tables")
@@ -115,7 +116,18 @@ class DMIinfo:
             rep_n.newProp("not_available", "1")
         else:
             self.__dmixml.SetResultType(dmidecode.DMIXML_DOC)
-            dmiqry = xmlout.convert_libxml2_to_lxml_doc(self.__dmixml.QuerySection('all'))
+            try:
+                dmiqry = xmlout.convert_libxml2_to_lxml_doc(self.__dmixml.QuerySection('all'))
+            except Exception as ex1:
+                self._log.log(Log.DEBUG, f'** EXCEPTION {str(ex1)}, will query BIOS only')
+                try:
+                    # If we can't query 'all', at least query 'bios'
+                    dmiqry = xmlout.convert_libxml2_to_lxml_doc(self.__dmixml.QuerySection('bios'))
+                except Exception as ex2:
+                    rep_n.addContent("No DMI tables available")
+                    rep_n.newProp("not_available", "1")
+                    self._log.log(Log.DEBUG, f'** EXCEPTION {str(ex2)}, dmi info will not be reported')
+                    return rep_n
             resdoc = self.__xsltparser(dmiqry)
             dmi_n = xmlout.convert_lxml_to_libxml2_nodes(resdoc.getroot())
             rep_n.addChild(dmi_n)
-- 
2.39.0

